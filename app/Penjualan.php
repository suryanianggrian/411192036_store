<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Penjualan extends Model
{
    protected $table = 'penjualan';
    protected $primarykey = 'id';

    protected $fillable = [
        'no_penjualan', 
        'tanggal', 
        'kode_pelanggan', 
        'kode_barang', 
        'jumlah_barang', 
        'harga_barang'
    ];
}