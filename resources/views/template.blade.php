<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>UTS_411192036</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Material+Symbols+Outlined:opsz,wght,FILL,GRAD@48,400,0,0" />
</head>
<body style="background: lightblue">
    <div class="container" >
        <div class="container card-body">
            <a href="{{ route('barang.index') }}" class="btn btn-sm btn-primary">Data Barang</a>
            <a href="{{ route('pelanggan.index') }}" class="btn btn-sm btn-primary">Data Pelanggan</a>
            <a href="{{ route('penjualan.index') }}" class="btn btn-sm btn-primary">Data Penjualan</a>
        </div>
        <br>
        <div class="container">
            @yield('content')
        </div>
    </div>
</body>
</html>